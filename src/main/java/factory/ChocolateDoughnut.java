package factory;

public class ChocolateDoughnut implements Doughnut{
    public void eat() {
        System.out.println("You are eating Chocolate Doughnut!");
    }
}
