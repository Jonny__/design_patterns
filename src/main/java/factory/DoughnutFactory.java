package factory;

public class DoughnutFactory {
    public Doughnut getDoughnut(DoughnutTypes type) {
        Doughnut doughnut = null;
        switch (type) {
            case CHERRY:
                doughnut = new CherryDoughnut();
                break;
            case CHOCOLATE:
                doughnut = new ChocolateDoughnut();
                break;
            case ALMOND:
                doughnut = new AlmondDoughnut();
                break;
            default:
                throw new IllegalArgumentException("Wrong doughnut type:" + type);
        }
        return doughnut;
    }
}
