package factory;

public class AlmondDoughnut implements Doughnut{
    public void eat() {
        System.out.println("You are eating Almond Doughnut!");
    }
}
