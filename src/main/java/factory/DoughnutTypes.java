package factory;

public enum DoughnutTypes {
    CHERRY,
    CHOCOLATE,
    ALMOND
}
